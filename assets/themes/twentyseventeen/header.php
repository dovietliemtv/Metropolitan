<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

	<div class="menu_laibunfu">
<div class="logo">
	<a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_site_url(); ?>/assets/uploads/2017/06/logo.png" Alt="Logo Laibunfu"></a>
</div>
<nav class="dt-main-menu_laibunfu">

	<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</nav><!-- .dt-main-menu -->
<div class="icon-social">
	<ul>
		<li><a href="#"><img src="<?php echo get_site_url(); ?>/assets/uploads/2017/06/facebook.png"></a></li>
		<li><a href="#"><img src="<?php echo get_site_url(); ?>/assets/uploads/2017/06/twitter.png"></a></li>
		<li><a href="#"><img src="<?php echo get_site_url(); ?>/assets/uploads/2017/06/pinterest.png"></a></li>
		<li><a href="#"><img src="<?php echo get_site_url(); ?>/assets/uploads/2017/06/flickr.png"></a></li>
	</ul>
</div>
</div>

	<div class="site-content-contain">
		<div id="content" class="site-content">
