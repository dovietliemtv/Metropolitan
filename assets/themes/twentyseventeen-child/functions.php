<?php

// enqueue the child theme stylesheet

Function wp_schools_enqueue_scripts() {
wp_register_style( 'childstyle', get_stylesheet_directory_uri() . '/style.css'  );
wp_enqueue_style( 'childstyle' );
}
add_action( 'wp_enqueue_scripts', 'wp_schools_enqueue_scripts', 11);
register_nav_menus( array(
    'main-menu' => 'Main menu',
) );

function menu_content_func() {
    ob_start();
    ?>
    <div class="container_laibunfu_2 menu">
        <div class="content_laibunfu">
            <div class="gallery_laibunfu menu-content menu-content1 active">
                <h3>Metropolitan Ala Carte Lunch Menu</h3>

                <h4>“First bites are the best”</h4>

                <p>
                    <strong>Charcuterie Plate - 240.000</strong>
                    A combination of Spanish Serrano, French Sauscison, Italian Salumi
                    <em>Đĩa thịt nguội tổng hợp. Giăm Bông Heo Tây Ban Nha, Xúc Xích khô Pháp, Xúc xích tỏi Ý</em>
                </p>

                <p>
                    <strong>Prime steak Carpaccio, saved parmesan reggiano, rockets & vinaigrette. – 230.000</strong>
                    Bò Mỹ Cao Cấp dùng Tái, Phô Mai Parmesan, Rau Mù tạc và dầu dấm.
                </p>

                <p><strong>
                        * Perfect egg with wild mushrooms – 120.000
                    </strong>
                    Trứng Hồng Đào nấu chậm với nấm rừng.
                </p>

                <p>
                    <strong>
                        Metropolitan Slider, Kale Salad. (2 pcs slider) – 110.000
                    </strong>
                    Bánh Bơ-gơ nhỏ kiểu Metropolitan, Xà lách Cải Xoắn. (2 bánh nhỏ).
                </p>

                <p>
                    <strong> Maryland style white lump crab cakes, chive buerre blanc. (2 pcs for App.) - 180.000</strong>
                    Bánh cua kiểu Maryland, Sốt bơ dấm rượu.(2 bánh cho Khai Vị)
                </p>

                <p>
                    <strong> Traditional Gravlax Salmon, Capers and Pickle Red Onion. – 150.000</strong>
                    Cá Hồi Muối, Nụ Bạch Hoa và Hành tây tím chua.
                </p>




                <h4>“Sentual Soups & Spectacular Salads”</h4>
                <p>
                    <strong>* Red dragon fruit gazpacho, ricotta, toasted pumpkin seeds, and Greek olive oil. – 120.000</strong>
                    Súp lạnh Thanh Long Đỏ, phô mai Ricotta, hạt bí nướng và Dầu Ôliu Hy Lạp.
                </p>



                <p>
                    <strong>White bean and lotus soup, Crisp Pancetta, Himalayan salt, Phu Quoc peppercorns with essence of White Alba Truffles – 120.000</strong>
                    Súp Đậu Trắng và Hạt Sen, Ba rọi giòn, Muối Hồng Hymalaya, Tiêu Phú Quốc với Dầu nấm Truffle Trắng.
                </p>

                <p>
                    <strong>
                        French onion soup with Emmential cheese and Puff Pastry. – 130.000
                    </strong>
                    Súp Hành Kiểu Pháp với phô mai Emmential và Bánh bơ nhiều lớp.
                </p>

                <p>
                    <strong>
                        New England style clam chowder, grilled garlic bread.  – 150.000
                    </strong>
                    Súp Nghêu kiểu Anh, Bánh mì tỏi.
                </p>

                <p>
                    <strong>
                        * Watermelon Greek Salad with Feta Cheese & Pickle Red Onion. – 120.000
                    </strong>
                    Xà lách dưa hấu Hy Lạp với phô mai Feta & hành tây tím ngâm chua.
                </p>


                <p>
                    <strong>
                        * Metropolitan salad - Organic Kale salad, toasted almonds, tomato, bacon with honey-mustard vinaigrette. – 120.000
                    </strong>
                    Xà lách Metropolitan - xà lách cải xoắn, hạnh nhân nướng, cà chua, ba rọi xông khói với dầu dấm mật ong mù tạc.
                </p>

                <h4>“Pasta”</h4>
                <p>
                    <strong>
                        Spaghetini with Sicilian Style Bolognese	– 280.000
                    </strong>
                    Mì Ý sợi tròn với Sốt Bò Bằm kiểu Địa Trung Hải
                </p>


                <p>
                    <strong>
                        Fettuccine, Fresh Carbonara, Pancetta and shaved Parmesan – 280.000
                    </strong>
                    Mì Ý sợi dẹp, Sốt kem tươi, Ba rọi xông khói và Phô mai Parmesan
                </p>

                <p>
                    <strong>
                        * Linguine Ala Tuscana – 280.000
                    </strong>
                    Mì Ý sợi thoi, rau củ và ôliu Kalamata
                </p>

                <h4>“Signature dishes, Poultry & Seafood”</h4>

                <p>
                    <strong>
                        Metropolitan USDA Beef Burger, Kale Salad – 160.000
                    </strong>
                    Bánh Bơ-gơ Bò Mỹ Metropolitan, Xà lách cải xoắn.
                </p>


                <p>
                    <strong>
                        Crispy Skin Salmon with Mediterranean Broth – 350.000
                    </strong>
                    Cá Hồi Da Giòn với nước súp kiểu Địa Trung Hải.
                </p>


                <p>
                    <strong>
                        Simply Grilled Blue River Prawns, Peri Peri sauce – 420.000
                    </strong>
                    Tôm Càng Sông Nướng. Sốt Nam Phi Peri Peri.
                </p>
                <p>
                    <strong>
                        Maryland style white lump crab cakes, chive buerre blanc. (3 pcs for Main) – 280.000
                    </strong>
                    Bánh cua kiểu Maryland, Sốt bơ dấm rượu.(3 bánh cho Món Chính).
                </p>

                <p>
                    <strong>
                        Fall of the bone Asian braised short ribs – 380.000
                    </strong>
                    7 hour braised short ribs with touches of Asian spice
                    <em> Sườn Bò Châu Á Om. Sườn bò om 7 giờ với các loại hương vị Châu Á.</em>
                </p>



                <p>
                    <strong>Metropolitan Flat Iron Flank Steak (250 grams) – 480.000</strong>
                    Marinated in soy, garlic, ginger and dried Hunan peppers, this cut takes you around Asia without leaving your chair
                    <strong> Thăng bụng Bò Angus Nướng Chảo Gang Metropolitan.</strong>
                    Ướp với Nước tương, tỏi, gừng và hạt tiêu Hunan, với món bít-tết này chúng tôi sẽ đưa quí vị đi vòng quanh Châu Á mà không cần rời khỏi ghế.
                </p>


                <h4>“Sauce”</h4>

                <p>
                    <strong>
                        * Horseradish Cream – 40.000
                    </strong>
                    Sốt Kem Đậu Răng Ngựa.
                </p>


                <p>
                    <strong>
                        Port Wine Reduction – 40.000
                    </strong>
                    Sốt Rượu Đỏ Proto Cô Đặc
                </p>

                <p>
                    <strong>
                        Phu Quoc Peppercorn  – 40.000
                    </strong>
                    Sốt tiêu Phú Quốc.
                </p>

                <p>
                    <strong>
                        * Peri Peri ( spicy) – 40.000
                    </strong>
                    Sốt cay Peri Peri
                </p>

                <h4>“Sidekicks”</h4>

                <p>
                    <strong>
                        Steamed Dalat Asparagus and Serrano ham – 60.000
                    </strong>
                    Măng tây Đà Lạt luộc và Đùi heo muối
                </p>

                <p>
                    <strong>
                        Sweet Can Tho corn succotash with pancetta – 60.000
                    </strong>
                    Đậu Ngô Cần Thơ với Ba rọi xông khói.
                </p>

                <p>
                    <strong>
                        * Chicago style roasted garlic mashed potatoes – 60.000
                    </strong>
                    Khoai Tây Nghiền Tỏi kiểu Chicago
                </p>

                <p>
                    <strong>
                        Lyonnaise Potatoes – 60.000
                    </strong>
                    Khoai tây kiểu Lyon
                </p>

                <p>
                    <strong>
                        * Sautéed wild mushrooms and garlic – 60.000
                    </strong>
                    Nấm rừng xào tỏi.
                </p>

                <p>
                    <strong>
                        * Caramelized cauliflower and pepper flakes. – 60.000
                    </strong>
                    Bông cải trắng Caramen và tiêu hột.
                </p>


                <h4>“Dessert”</h4>
                <p>
                    <strong>
                        * New York Cheese Cake. – 110.000
                    </strong>
                    Bánh Kem phô mai New York.
                </p>

                <p>
                    <strong>
                        * Classic Tiramusu. – 120.000
                    </strong>
                    Bánh Tiramisu Cổ Điển.
                </p>

                <p>
                    <strong>
                        * Chocolate Mousse. – 90.000
                    </strong>
                    Bánh Sô-cô-la mịn.
                </p>


                <p>
                    <strong>
                        * Classic Crème Bruleé. – 90.000
                    </strong>
                    Bánh trứng Bỏ lò kiểu cổ điển.
                </p>
                <p>
                    <strong>
                        * Seasonal Fresh Fruit. – 200.000
                    </strong>
                    Trái cây tươi theo mùa.
                </p>

                <p>* vegetarian options</p>
                <p>~Lunch service is from Monday thru Friday~</p>

            </div>
            <div class="gallery_laibunfu menu-content menu-content2">
                <h3>SET MENU FOR LUNCH</h3>
                200.000 VND ++/ set ( 3 Course).

                <h4>~ First Course : Soup & Salad ~</h4>

                <p>
                    <strong>
                        *RED DRAGON FRUIT GAZPACHO, RICOTTA, TOASTED PUMPKIN SEEDS, AND GREEK OLIVE OIL.
                    </strong>
                    Súp lạnh Thanh Long Đỏ, phô mai Ricotta, hạt bí nướng và Dầu Ôliu Hy Lạp.
                </p>
                <p>
                    <strong>
                        WHITE BEAN AND LOTUS SOUP, CRISP PANCETTA, HIMALAYAN SALT, PHU QUOC PEPPERCORNS WITH ESSENCE OF WHITE ALBA TRUFFLES.
                    </strong>
                    Súp Đậu Trắng và Hạt Sen, Ba rọi giòn, Muối Hồng Hymalaya, Tiêu Phú Quốc với Dầu nấm Truffle Trắng.
                </p>

               <p>
                   <strong>
                       *PERFECT EGG WITH WIND MUSHROOM
                   </strong>
                   Trứng Hồng Đào nấu chậm với nấm rừng.
               </p>

                <p>
                    <strong>
                        *WATERMELON GREEK SALAD WITH FETA CHEESE & PICKLE RED ONION.
                    </strong>
                    Xà lách dưa hấu Hy Lạp với phô mai Feta & hành tây tím ngâm chua.
                </p>


                <p>
                    <strong>
                        *METROPOLITAN SALAD - ORGANIC KALE SALAD, TOASTED ALMONDS, TOMATO WITH HONEY-MUSTARD VINAIGRETTE.
                    </strong>
                    Xà lách Metropolitan - xà lách cải xoắn, hạnh nhân nướng, cà chua với dầu dấm mật ong mù tạc.
                </p>


                <h4>~Second Course : Main Course~</h4>

                <p>
                    <strong>
                        METROPOLITAN SLIDER, KALE SALAD. ( 2 PCS SLIDER) ( Choice Beef, Crab Cakes or both).
                    </strong>
                    Bánh Bơ-gơ nhỏ kiểu Metropolitan, Xà lách Cải Xoắn. ( 2 bánh nhỏ)( Chọn Bò, Bánh Cua hoặc cả 2).
                </p>
                <p>
                    <strong>
                        NEW ENGLAND STYLE CLAM CHOWDER, GRILLED GARLIC BREAD.
                    </strong>
                    Súp Nghêu kiểu Anh, Bánh mì tỏi.
                </p>
                <p>
                    <strong>
                        METROPOLITAN SOUVLAKI
                    </strong>
                    Thăng bụng Bò Angus xiên que Metropolitan.
                </p>


                <p>
                    <strong>
                        *LINGUINI ALA TUSCANA
                    </strong>
                    Mì ý sợi thoi, rau củ và ôliu Kalamata..
                </p>

                <p>
                    <strong>
                        SPAGHETINI WITH SICILAN STYLE BOLOGNESE
                    </strong>
                    Mì ý sợi tròn với Sốt Bò Bằm kiểu Địa Trung Hải
                </p>



                <h4>~Third Course: Dessert~</h4>

                <p>
                    <strong>
                        *CLASSIC CRÈME BRULEÉ.
                    </strong>
                    Bánh trứng Bỏ lò kiểu cổ điển.
                </p>

                <p>
                    <strong>
                        *SEASONAL FRESH FRUIT.
                    </strong>
                    Trái cây tươi theo mùa.
                </p>

                <p><strong>*vegetarian option</strong></p>

                <p> ~Lunch service is only available from Monday thru Friday~</p>

            </div>
            <div class="gallery_laibunfu menu-content menu-content3">
                <h3>Metropolitan Grill Dinner Menu</h3>

                <h4>“First bites are the best”</h4>

                <p>
                    <strong>
                        Charcuterie Plate - 240.000
                    </strong>
                    A combination of Spanish Serrano, French Sauscison, Italian Salumi
                    <em>
                        Đĩa thịt nguội tổng hợp. Giăm Bông Heo Tây Ban Nha, Xúc Xích khô Pháp, Xúc xích tỏi Ý
                    </em>
                </p>

                <p>
                    <strong>
                        Prime steak Carpaccio, saved parmesan reggiano, rockets & vinaigrette. – 230.000
                    </strong>
                    Bò Mỹ Cao Cấp dùng Tái, Phô Mai Parmesan, Rau Mù tạc và dầu dấm.
                </p>
                <p>
                    <strong>
                        * Perfect egg with wild mushrooms – 120.000
                    </strong>
                    Trứng Hồng Đào nấu chậm với nấm rừng.
                </p>

                <p>
                    <strong>
                        Metropolitan Slider, Kale Salad. (2 pcs slider) – 110.000
                    </strong>
                    Bánh Bơ-gơ nhỏ kiểu Metropolitan, Xà lách Cải Xoắn. (2 bánh nhỏ).
                </p>


                <p>
                    <strong>
                        Maryland style white lump crab cakes, chive buerre blanc. (2 pcs for App.) - 180.000
                    </strong>
                    Bánh cua kiểu Maryland, Sốt bơ dấm rượu.(2 bánh cho Khai Vị)
                </p>
                <p>
                    <strong>
                        Traditional Gravlax Salmon, Capers and Pickle Red Onion. – 150.000
                    </strong>
                    Cá Hồi Muối, Nụ Bạch Hoa và Hành tây tím chua.
                </p>

                <h4>“Sentual Soups & Spectacular Salads”</h4>

                <p>
                    <strong>
                        * Red dragon fruit gazpacho, ricotta, toasted pumpkin seeds, and Greek olive oil. – 120.000
                    </strong>
                    Súp lạnh Thanh Long Đỏ, phô mai Ricotta, hạt bí nướng và Dầu Ôliu Hy Lạp.
                </p>

                <p>
                    <strong>
                        White bean and lotus soup, Crisp Pancetta, Himalayan salt, Phu Quoc peppercorns with essence of White Alba Truffles – 120.000
                    </strong>
                    Súp Đậu Trắng và Hạt Sen, Ba rọi giòn, Muối Hồng Hymalaya, Tiêu Phú Quốc với Dầu nấm Truffle Trắng.
                </p>

                <p>
                    <strong>
                        French onion soup with Emmential cheese and Puff Pastry. – 130.000
                    </strong>
                    Súp Hành Kiểu Pháp với phô mai Emmential và Bánh bơ nhiều lớp.
                </p>

                <p>
                    <strong>
                        New England style clam chowder, grilled garlic bread.  – 150.000
                    </strong>
                    Súp Nghêu kiểu Anh, Bánh mì tỏi.
                </p>

                <p>
                    <strong>
                        * Watermelon Greek Salad with Feta Cheese & Pickle Red Onion. – 120.000
                    </strong>
                    Xà lách dưa hấu Hy Lạp với phô mai Feta & hành tây tím ngâm chua.
                </p>

                <p>
                    <strong>
                        * Metropolitan salad - Organic Kale salad, toasted almonds, tomato, bacon with honey-mustard vinaigrette. – 120.000
                    </strong>
                    Xà lách Metropolitan - xà lách cải xoắn, hạnh nhân nướng, cà chua, ba rọi xông khói với dầu dấm mật ong mù tạc.
                </p>


                <h4>“The cuts”</h4>

                <p>
                    <strong>
                        Our steaks are hand carved and aged in-house for at least 21 days to accentuate their flavors and aromas.
                    </strong>
                    Tất cả bít-tết của chúng tôi được tự tay đầu bếp ướp khô trong ít nhất 21 ngày để làm nổi bật hương vị và mùi hương của chúng.
                </p>

                <p>
                    <strong>
                        USDA Cajun Style Oyster Blade (300 grams) – 450.000
                    </strong>
                    Lõi Vai Bò Mỹ kiểu Cajun
                </p>

                <p>
                    <strong>
                        Metropolitan Flat Iron Flank Steak (250 grams) – 480.000
                    </strong>
                    Marinated in soy, garlic, ginger and dried Hunan peppers, this cut takes you around Asia without leaving your chair
                </p>
                <p>
                    <strong>
                        Thăng bụng Bò Angus Nướng Chảo Gang Metropolitan.
                    </strong>
                    Ướp với Nước tương, tỏi, gừng và hạt tiêu Hunan, với món bít-tết này chúng tôi sẽ đưa quí vị đi vòng quanh Châu Á mà không cần rời khỏi ghế.
                </p>

                <p>
                    <strong>
                        Entrecote Style USDA Prime Striploin (250 grams) – 650.000
                    </strong>
                    Enjoy what the French have been eating for centuries, this cut is accentuated with Metropolitan’s signature Mustard Bordelaise sauce
                    <strong>
                        Thăn lưng Bò Angus kiểu Entrecote.
                    </strong>
                    Thưởng thức những gì mà người Pháp từng ăn qua từng thế kỉ, phần Bít-tết này được làm nổi bật với hương vị đặc trưng của sốt Mù Tạc Bordelaise.
                </p>

                <p>
                    <strong>
                        Chicago Style USDA Prime Ribeye (400 grams) ( 21 day Dry-Age) – 950.000
                    </strong>
                    Go back in time where chivalry still opened doors for ladies and cuts like this one ruled the steakhouse, a class cut boasting flavor and tenderness all at the same time
                    <strong>
                        Nạc Lưng Bò Mỹ kiểu Chicago ( Ướp khô 21 ngày).
                    </strong>
                    Quay trở lại thời gian, nơi mà sự hào hoa mở cửa cho phụ nữ, và Bít-tết này đã ngự trị các nhà hàng Bít-tết, 1 loại Bít-tết mang đến hương vị dịu dàng và mềm mại
                </p>
                <p>
                    <strong>
                        Signature Cut USDA Prime New York Strip (300 grams) (21 day Dry-Age) – 850.000
                    </strong>
                    Deep in flavor with ample marbling, this cut was Sir Winston Churchill’s favorite
                    <strong>
                        Thăn Lưng Bò Mỹ Cao Cấp Đặc Trưng ( Ướp khô 21 ngày).
                    </strong>
                    Hương vị đậm đà với những thớ thịt và mỡ đều nhau, Bít – tết này được Sir Winston Churchill thích nhất.
                </p>


                <p>
                    <strong>
                        Double Cut USDA Ribeye to share (900 grams) (21 day Dry-Age) – 2.150.000
                    </strong>
                    Majestic and awe inspiring, this cut presented at the table will be the talk of the town and enough for 4 people
                    <strong>
                        “Trọng lượng gấp đôi” Nạc lưng Bò Mỹ dùng chung (900 grams) (Ướp khô 21 ngày).
                    </strong>
                    Với khối lượng ấn tượng và đẳng cấp, món ăn phục vụ trên bàn sẽ là một đề tài để bàn tán trong câu chuyện hằng ngày. Món bít-tết này phù hợp khẩu phần cho 4 người.
                </p>

                <p>
                    <strong>
                        Metropolitan Double Cut Pork Chop (350 grams) – 370.000
                    </strong>
                    Brined for 3 days in botanicals, seared to perfection and served with grilled asparagus, this cut will leave you breathless
                    <strong>
                        Cốt lếch Heo trọng lượng gấp đôi Metropolitan.
                    </strong>
                    Được xử lý trong 3 ngày với các loại thực vật, áp chảo hoàn hảo và phục vụ với măng tây Đà Lạt
                </p>


                <h4>“Signature dishes, Poultry & Seafood”</h4>

                <p>
                    <strong>
                        Metropolitan USDA Beef Burger, Kale Salad – 160.000
                    </strong>
                    Bánh Bơ-gơ Bò Mỹ Metropolitan, Xà lách cải xoắn.
                </p>


                <p>
                    <strong>
                        Crispy Skin Salmon with Mediterranean Broth – 350.000
                    </strong>
                    Cá Hồi Da Giòn với nước súp kiểu Địa Trung Hải.
                </p>


                <p>
                    <strong>
                        Simply Grilled Blue River Prawns, Peri Peri sauce – 420.000
                    </strong>
                    Tôm Càng Sông Nướng. Sốt Nam Phi Peri Peri.
                </p>

                <p>
                    <strong>
                        Fall of the bone Asian braised short ribs – 380.000
                    </strong>
                    7 hour braised short ribs with touches of Asian spice
                    <em> Sườn Bò Châu Á Om. Sườn bò om 7 giờ với các loại hương vị Châu Á.</em>
                </p>

                <p>
                    <strong>
                        Maryland style white lump crab cakes, chive buerre blanc. (3 pcs for Main) – 280.000
                    </strong>
                    Bánh cua kiểu Maryland, Sốt bơ dấm rượu.(3 bánh cho Món Chính).
                </p>


                <h4>“Pasta”</h4>
                <p>
                    <strong>
                        Spaghetini with Sicilian Style Bolognese	– 280.000
                    </strong>
                    Mì Ý sợi tròn với Sốt Bò Bằm kiểu Địa Trung Hải
                </p>
                <p>
                    <strong>
                        Fettuccine, Fresh Carbonara, Pancetta and shaved Parmesan – 280.000
                    </strong>
                    Mì Ý sợi dẹp, Sốt kem tươi, Ba rọi xông khói và Phô mai Parmesan
                </p>
                <p>
                    <strong>
                        * Linguine Ala Tuscana – 280.000
                    </strong>
                    Mì Ý sợi thoi, rau củ và ôliu Kalamata
                </p>

                <h4>“Sauce”</h4>
                <p>
                    <strong>
                        * Horseradish Cream – 40.000
                    </strong>
                    Sốt Kem Đậu Răng Ngựa.
                </p>
                <p>
                    <strong>
                        Port Wine Reduction – 40.000
                    </strong>
                    Sốt Rượu Đỏ Proto Cô Đặc
                </p>

                <p>
                    <strong>
                        Phu Quoc Peppercorn  – 40.000
                    </strong>
                    Sốt tiêu Phú Quốc.
                </p>


                <p>
                    <strong>
                        * Peri Peri ( spicy) – 40.000
                    </strong>
                    Sốt cay Peri Peri
                </p>

                <h4>“Sidekicks”</h4>
                <p>
                    <strong>
                        Steamed Dalat Asparagus and Serrano ham – 60.000
                    </strong>
                    Măng tây Đà Lạt luộc và Đùi heo muối
                </p>

                <p>
                    <strong>
                        Sweet Can Tho corn succotash with pancetta – 60.000
                    </strong>
                    Đậu Ngô Cần Thơ với Ba rọi xông khói.
                </p>


                <p>
                    <strong>
                        * Chicago style roasted garlic mashed potatoes – 60.000
                    </strong>
                    Khoai Tây Nghiền Tỏi kiểu Chicago
                </p>


                <p>
                    <strong>
                        Lyonnaise Potatoes – 60.000
                    </strong>
                    Khoai tây kiểu Lyon
                </p>

                <p>
                    <strong>
                        * Sautéed wild mushrooms and garlic – 60.000
                    </strong>

                    Nấm rừng xào tỏi.
                </p>


                <p>
                    <strong>
                        * Caramelized cauliflower and pepper flakes. – 60.000
                    </strong>

                    Bông cải trắng Caramen và tiêu hột.
                </p>

                <h4>“Dessert”</h4>

                <p>
                    <strong>
                        * New York Cheese Cake. – 110.000
                    </strong>
                    Bánh Kem phô mai New York.
                </p>
                <p>
                    <strong>
                        * Classic Tiramusu. – 120.000
                    </strong>
                    Bánh Tiramisu Cổ Điển.
                </p>
                <p>
                    <strong>
                        * Chocolate Mousse. – 90.000
                    </strong>
                    Bánh Sô-cô-la mịn.
                </p>

                <p>
                    <strong>
                        * Classic Crème Bruleé. – 90.000
                    </strong>
                    Bánh trứng Bỏ lò kiểu cổ điển.
                </p>

                <p>
                    <strong>
                        * Seasonal Fresh Fruit. – 200.000
                    </strong>
                    Trái cây tươi theo mùa.
                </p>
                <p>* vegetarian options</p>
            </div>
        </div>
    </div>

    <?php
    return ob_get_clean();
}

add_shortcode( 'menu-content', 'menu_content_func' );



function membership_content_func() {
    ob_start();
    ?>
    <div class="container_laibunfu_2">
        <div class="content_laibunfu">
            <div class="gallery_laibunfu membership-content membership-content1 active">
                <h3>Silver membership</h3>
                <p>The world awaits…with the silver memberships, the card holder can enjoy the following benefits when they present their card at the Met:</p>
                <ul>
                    <li>10% reduction on all food menu items</li>
                    <li>20% reduction on all our cocktails, beers, water/coffee and House wine list</li>
                    <li>Able to order from the Blacklist</li>
                    <li>Corkage for wine will be reduced to 200,000vnd/bottle</li>
                    <li>Corkage for spirits will be reduced to 700,000vnd/bottle</li>
                    <li>Invitation to our monthly wine events</li>
                    <li>Enrolled in our monthly newsletter</li>
                </ul>
                <p>Annual subscription is 5,000,000vnd</p>
                <p>*membership is non-transferrable</p>

            </div>
            <div class="gallery_laibunfu membership-content membership-content2">
                <h3>Gold membership</h3>
                <p>
                    The color of El Dorado, this membership will allow you to spread your wings and soar through the Metropolitan with greater freedom than you can imagine.
                    And while you are sky high, enjoy these benefits:
                </p>


                <ul>
                    <li>10% reduction on all food menu items</li>
                    <li>20% reduction on all our cocktails, beers, water/coffee and House wine list</li>
                    <li>Able to order from the Blacklist</li>
                    <li>Minimum charge on the Private room is reduced to 10,000,000vnd</li>
                    <li>Corkage for wine will be reduced to 200,000vnd/bottle</li>
                    <li>Corkage for spirits will be reduced to 500,000vnd/bottle</li>
                    <li>Invitation to our monthly wine events</li>
                    <li>Enrolled in our monthly newsletter</li>
                </ul>
                <p> Annual subscription is 10,000,000vnd</p>
                <p> *membership is non-transferrable</p>


            </div>
            <div class="gallery_laibunfu membership-content membership-content3">
                <h3>Diamond membership</h3>
                <p>
                    The hardest substance in the world and able to cut through anything, this all encompassing Diamond membership at the Metropolitan will allow
                    you to move throughout Asia and still feel you are at the Met.  With reciprocating benefits from places as such as the Jockey Club HK, the Peninsula HK,
                    AnOther Place HK, Shangri-la HK, Cristal Jade Singapore etc., the jet setting life awaits and the G5 is refueling.  Here’s to champagne wishes and caviar dreams.
                </p>

                <ul>
                    <li>15% reduction on all food menu items</li>
                    <li>20% reduction on all our cocktails, beers, water/coffee and House wine list</li>
                    <li>10% reduction on all wines from the Blacklist</li>
                    <li>Able to keep 6 bottles of wine in our Eurocave at the Met for 6 months</li>
                    <li>Minimum charge on the Private room is reduced to 10,000,000vnd</li>
                    <li>No corkage on the first 5 bottles of wine, 200,000vnd for any bottles there after</li>
                    <li>No corkage on the first bottle of spirit, 500,000vnd/bottle for any bottles there after</li>
                    <li>Invitation to our monthly wine events with priority seating</li>
                    <li>Enrolled in our monthly newsletter</li>
                </ul>
                <p>Annual subscription is 20,000,000vnd</p>
                <p>*membership is non-transferrable</p>

            </div>
        </div>
    </div>

    <?php
    return ob_get_clean();
}

add_shortcode( 'membership-content', 'membership_content_func' );

function wines_content_func() {
    ob_start();
    ?>
    <div class="container_laibunfu_2 wines">
        <div class="content_laibunfu">
            <div class="gallery_laibunfu wines-content wines-content1 active">
                <h3>WINE BY THE GLASS </h3>
                <div class="unit">
                    <div class="unit-col">VND</div>
                    <div class="unit-col">150ML/GLS</div>
                    <div class="unit-col">BOTTLE</div>
                </div>
                <h4>WHIMSICAL WHITES...</h4>
                <div class="group-menu">
                    <span>2016 Muscadet, Sauvion France </span>
                    <span>140 </span>
                    <span>750</span>
                    <p>Reflections of the Seaside air- Zesty lemons, oyster shells, and a kiss of sourdough bread.
                        Pair it with Signature Clam Chowder, Maryland Crab Cakes, with a side of Corn Succotash with pancetta.
                    </p>
                </div>

                <div class="group-menu">
                    <span>  2016 Riesling, Schloss Vollrads “Sommer”, Germany   </span>
                    <span>160 </span>
                    <span>780</span>
                    <p>Reflections of the Seaside air- Zesty lemons, oyster shells, and a kiss of sourdough bread.
                        Pair it with Signature Clam Chowder, Maryland Crab Cakes, with a side of Corn Succotash with pancetta.
                    </p>
                </div>
                <div class="group-menu">
                    <span>    2017 Sauvignon Blanc, Mussel Bay, New Zealand   </span>
                    <span>150 </span>
                    <span>780</span>
                    <p>
                        Highly aromatic dance. Passionfruit, Grapefruit, and gooseberries mesh with fresh coriander, asparagus, and geranium flowers.
                        Best bet to win with Metropolitan Salad, side of Asparagus with Serrano ham, and Linguine Tuscana.
                    </p>
                </div>

                <div class="group-menu">
                    <span> 2014 Chardonnay, Joseph Drouhin Macon Lugny, France   </span>
                    <span>160 </span>
                    <span>850</span>
                    <p>
                        Perfect capture of the Burgundian landscape- Complex and multi-layered. Ripe Quince, fresh fig,
                        and concentrated citrus meets a hint of chalky organic earth into a vanilla buttercream long persistence.
                        Simply cannot miss with Maryland Crab Cakes, Clam Chowder, Fettucine, and side mushrooms with garlic. Oh shoot, an order of Truffle fries too please!
                    </p>
                </div>
                <div class="group-menu">
                    <span>  2016 Arneis di Roero, Prunotto Italy   </span>
                    <span>160 </span>
                    <span>850</span>
                    <p>
                        icknamed "Little Rascal" by the Italians due to its finicky growing into grapes in the vineyard.
                        This rascal shows ripe pear and fresh apples with a slight fennel seed, almond skin,and an oily textured finish.
                        Celebrate the day with Crispy Salmon in Meditteranean broth, White Bean and Lotus Soup,
                        and don't forget the Lyonnaise potatoes!</p>
                </div>


              <!--  <h4>SPARKLING</h4>
                <div class="group-menu">
                    <span> 2016 Prosecco, Zonin Cuvee 1821 MV Italy (375ml size)  </span>
                    <span>150 </span>
                    <span>420</span>
                    <p>
                        Refreshing white peach and white flower blossoms. A creamy, slight almond flavor on finish. Perfect partner with Perfect Egg starter,
                        Blue River Prawns, & Fettucine pasta dishes.
                    </p>
                </div>

                <h4>RAVISHING REDS...</h4>
                <div class="group-menu">
                    <span> Pinot Noir, Moulin de Gassac, France   </span>
                    <span>150 </span>
                    <span>820</span>
                    <p>
                        Legendary producer from the South of France. A mosaic of red cherry, blackcurrant, fresh dug mushrooms, nutmeg,
                        and tobacco leaf toast. Eureka moment with Double cut Pork Chop. Fabulous with the Charcuterie plate and Cripsy skin salmon.
                    </p>
                </div>


                <div class="group-menu">
                    <span>  Tempranillo-Carignena, Torres “Sangre de Toro”, Spain  </span>
                    <span> 160</span>
                    <span>890</span>
                    <p>
                        Proud as a Flamenco and daring as a Bull run. Dark plums and cherries, garden soil, leather, and butter-cooked walnuts abound. Choose your partner to dance or run-
                    </p>
                </div>

                <div class="group-menu">
                    <span>2014 Malbec-Syrah-Bonarda, La Posta, Argentina   </span>
                    <span> 140</span>
                    <span>780</span>
                    <p>
                        Living the Argentine Andes Dream. Fleshy bold blackberries, dark cassis,
                        and dried gamey meats into a vanilla brown baking spice box and balnced body with rich texture. Immaculate match with the Ribeye, Prime New York Striploin, and the charcuterie plate.
                    </p>
                </div>

                <div class="group-menu">
                    <span> 2016 Sangiovese, Banfi Chianti 'Primavera Selection', Italy   </span>
                    <span>150 </span>
                    <span>780</span>
                    <p>
                        Not your wicker basket version! This classic Chianti proclaims a medley of red fcherry ruit tones with saddle leather and a hint of olive with fresh oregano.
                        Pisa styled pride when matched with Spaghetini
                    </p>
                </div>

                <div class="group-menu">
                    <span>Cabernet Sauvignon, Vina Maipo, “Vitral Reserva”, Chile   </span>
                    <span>140 </span>
                    <span>850</span>
                    <p>
                        Welcome to the spirit within Santiago's soul. Extracted cassis, prunes, grilled bell peppers, and fresh ground coffee with a touch of smoky oak toast finish.
                        Full bodied and flavorful- don't overthink this one. Try it with the Ribeye or New York Strip!
                    </p>
                </div>
-->
                <h4>ROSE</h4>
                <div class="group-menu">
                    <span>Grenache-Syrah, L'Ostal Rose, France   </span>
                    <span>150 </span>
                    <span>650</span>
                    <p>
                        Classic dry styled from the gorgeous Provence region. This expresses fersh orange zest, watermelon,
                        an essence of fresh rose petals, and a lsight oily richness to its clean finish. Terrific with the French Onion Soup, Watermelon Salad,
                        and Dragonfruit Gazpacho. It it another level with the Prime Steak carpaccio!
                    </p>
                </div>

                <h4>FANCIFUL FLIGHTS : 250</h4>
                <p>TWO WINE OF YOUR CHOICE, 2 WHITES, 2 REDS OR 1 OF EACH 90ML/GLASS</p>

               <!-- <h4>SPARKLING</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>Ayala, Brut Majeur, Champagne, France  </span>
                    <span>2.340</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> Billecart-Salmon, Grand Cru Blanc de Blancs, Champagne, France </span>
                    <span>2.817</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> Bollinger, Brut Special Cuvee, Champagne, France </span>
                    <span>2.618</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Veuve Clicquot Ponsardin, Brut, Champagne, France  </span>
                    <span>1.500</span>
                </div>

                <h4>SPARKLING ROSE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>Bollinger, Rose, Champagne, France  </span>
                    <span>2.764</span>
                </div>-->

               <!-- <h3>WHITE WINE</h3>

                <h4>AUSTRALIA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Elderton, Marsanne-Roussanne, Eden Valley </span>
                    <span>858</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2016 Tyrell's Old Winery, Sauvingon Blanc-Semillon, Limestone Coast </span>
                    <span>878</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Jim Barry, Riesling Lodge Hill , Clare Valley  </span>
                    <span>1.052</span>
                </div>

                <h4>CHILE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Concha Y Toro, Chardonnay “Reservado”, Central Valley  </span>
                    <span>816</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Miguel Torres, Sauvignon Blanc, Central Valley  </span>
                    <span>669</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Santa Digna, Chardonnay, Central Valley  </span>
                    <span>867</span>
                </div>


                <h4>NEW ZEALAND</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2016 Craggy Range, Sauvignon Blanc, Martinborough  </span>
                    <span>1.113</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2016 Kim Crawford , Sauvignon Blanc, Marlborough </span>
                    <span>1.458</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 Ribbonwood, Sauvignon Blanc, Marlborough</span>
                    <span>821</span>
                </div>

                <h4>FRANCE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> Albert Bichot, Bourgogne “Vieilles Vignes”, Burgundy</span>
                    <span>1.059</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Albert Bichot, Chablis, Burgundy </span>
                    <span>1.170</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Calvet, Bordeaux </span>
                    <span>570</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Chateau Villa Bel-Air, Graves, Bordeaux </span>
                    <span>1.580</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Domaine Chanson, Chablis “Montmains” 1er Cru, Burgundy </span>
                    <span>2.070</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Domaine Chanson, Pouilly-Fuisse, Burgundy </span>
                    <span>1.840</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Gustave Lorentz, Gewurztraminer, Alsace </span>
                    <span>1.320</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> Pascal Jolivet, Sancerre, Loire</span>
                    <span>1.040</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Pierre y Remy Gauthier, Cotes du Rhone “Les Heritiers” </span>
                    <span>693</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Sauvion, Sancerre, Loire </span>
                    <span>1.560</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Trimbach, Riesling, Alsace </span>
                    <span>1.480</span>
                </div>

                <h3>WHITE WINE</h3>
                <h4>ITALY</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Cantina Castelnouvo del Garda, Soave </span>
                    <span>550</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Villa Antinori, Tuscany </span>
                    <span>696</span>
                </div>-->

               <!-- <h3>ROSÉ WINE</h3>
                <h4>FRANCE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Domaine Ott, Cotes de Provence </span>
                    <span>1.560</span>
                </div>

                <div class="group-menu group-menu-onlybottle">
                    <span>L'Ostal Cazes, Pays d'Oc </span>
                    <span>782</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2014 Paul Jaboulet-Aine, Parallele 45, Rhone</span>
                    <span>970</span>
                </div>


                <h3>RED WINE</h3>
                <h4>ARGENTINA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Catena Zapata, Cabernet Sauvignon, Mendoza </span>
                    <span>1.300</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Finca Flichman, Cabernet Sauvignon “Roble”, Mendoza </span>
                    <span>650</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2016 La Finca , Malbec, Mendoza </span>
                    <span>650</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 La Posta, Malbec-Syrah-Bonarda “Tinto” , Mendoza </span>
                    <span>660</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Luca, Malbec-Cabernet blend “Beso de Dante”, Mendoza </span>
                    <span>2.322</span>
                </div>


                <h4>NEW ZEALAND</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Craggy Range, Bordeaux Blend Gimblett Gravels “Te Kahu”, Hawke's Bay </span>
                    <span>1.360</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Kim Crawford , Pinot Noir, Marlborough </span>
                    <span>1.300</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Ribbonwood, Pinot Noir, Marlborough </span>
                    <span>1.200</span>
                </div>

                <h4>AUSTRALIA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Elderton, Cabernet Sauvignon “High Altitude”, Eden Valley</span>
                    <span>980</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Torbreck, GSM “Steading”, Barossa Valley </span>
                    <span>3585</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Torbreck, Shiraz-Viognier “Descendant”, Barossa Valley </span>
                    <span>1962</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Tyrrell's, Cabernet Sauvignon “Lost Block”, Limestone Coast </span>
                    <span>1100</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Tyrrell's, Pinot Noir, Limestone Coast </span>
                    <span>1100</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Tyrrell's, Shiraz “Rufus Stone”, Heathcote </span>
                    <span>1598</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Vasse Felix, Cabernet Sauvignon, Margaret River </span>
                    <span>1520</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Yangarra Estate, GSM , McClaren Vale</span>
                    <span>1780</span>
                </div>


                <h4>USA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Freemark Abbey, Merlot, Napa Valley </span>
                    <span>1440</span>
                </div>

                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Kendall-Jackson, Cabernet Sauvignon, Sonoma County </span>
                    <span>1440</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 La Crema, Pinot Noir, Sonoma Coast </span>
                    <span>1440</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Woodbridge by Robert Mondavi, Merlot, California </span>
                    <span>858</span>
                </div>


                <h3>RED WINE</h3>
                <h4>CHILE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>Castillo de Molina, Shiraz, Maule Valley </span>
                    <span>987</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Chateau los Boldos , “ Grand Reserve, Central Valley </span>
                    <span>794</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Echeverria, Cabernet Sauvignon, Central Valley </span>
                    <span>643</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Lapostolle, Merlot, Colchagua Valley </span>
                    <span>869</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Mancura, Merlot “Etnia”, Central Valley </span>
                    <span>561</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Manso de Velasco, Cabernet Sauvignon, Curico </span>
                    <span>2.444</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Miguel Torres, Cabernet Sauvignon “Hemisferio”, Central Valley </span>
                    <span>669</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Miguel Torres, Cabernet Sauvignon “Las Mulas”, Central Valley </span>
                    <span>841</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Miguel Torres, Cabernet Sauvignon “Tormenta”, Central Valley </span>
                    <span>893</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Vina Bisquertt, Pinot Noir “La Joya”, Leyda Valley </span>
                    <span>880</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Vina Leyda, Pinot Noir “Las Brisas Vineyard”, Leyda Valley </span>
                    <span>1.100</span>
                </div>

                <h4>SPAIN</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>Descendientes de J. Palacios, Bierzo 'Petalos', Castille-Leon </span>
                    <span>1860</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 La Planta, Ribera del Duero, Castille-Leon</span>
                    <span>696</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Torres, Purgatori', Costers del Segre </span>
                    <span>1.962</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Marques de Caceres, Rioja 'Crianza', Rioja </span>
                    <span>537</span>
                </div>


                <h4>ITALY</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Mangnifico Fuoco, Primitivo di Manduria, Apulia </span>
                    <span>970</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Masseria Altemura, Negroamaro, Apulia </span>
                    <span>1.180</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2010 Marchesi di Barolo, Barolo 'Sarmassa', Piedmont</span>
                    <span>3.600</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Marchesi di Barolo, Barbera del Monferrato 'Maraia', Piedmont </span>
                    <span>1.100</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>MV Marchesi di Barolo, Nebbiolo d'Alba 'Michet', Piedmont </span>
                    <span>1.498</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> Prunotto, Barolo, Piedmont</span>
                    <span>1.920</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Lunate,Merlot-Nero d'Avola, Sicily </span>
                    <span>673</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Ca' Marcanda di Gaja, Promis' Bolgheri, Tuscany </span>
                    <span>2.594</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Castello d'Albola, Chianti Classico, Tuscany </span>
                    <span>1.318</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Le Volte dell'Ornellaia,Merlot-Sangiovese-Cabernet Sauvignon,Tuscany </span>
                    <span>1.360</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Marchese Antinori, Brunello di Montalcino 'Pian delle Vigne', Tuscany </span>
                    <span>2.688</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Marchese Antinori, Tignanello, Tuscany </span>
                    <span>3.712</span>
                </div>


                <h3>RED WINE</h3>
                <h4>FRANCE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Trimbach, Pinot Noir Classic, Alsace</span>
                    <span>1,680</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2012 Château Gruard-Larose, Saint-Julien 'Second Growth', Bordeaux</span>
                    <span>3,783</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Château Le Crock, Saint-Estèphe, Bordeaux </span>
                    <span>2,668</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2011 Château Villa Bel-Air Graves, Bordeaux</span>
                    <span>1,364</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 J.M. Cazes, Pauillac, Bordeaux </span>
                    <span>770</span>
                </div>

                <div class="group-menu group-menu-onlybottle">
                    <span>2012 La Réserve de Malartic-Lagraviere, Pessac-Leognan, Bordeaux </span>
                    <span>1,800</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Le Médoc de Cos d'Estournel, Saint-Estèphe, Bordeaux </span>
                    <span>2,609</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Mahler Besse 'Cheval Noir',Saint-Émilion Bordeaux </span>
                    <span>1,530</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 S de Siran, Margaux, Bordeaux </span>
                    <span>2,200</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2012 Albert Bichot, Cotes de Nuits-Villages, Burgundy</span>
                    <span>1,422</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Albert Bichot, Bourgogne 'Vieilles Vignes',Burgundy </span>
                    <span>1,059</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Domaine Chanson, Santenay, Burgundy </span>
                    <span>1,962</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Guigal, Crozes-Hermitage, Rhône</span>
                    <span>1,906</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Guigal, Cotes du, Rhône </span>
                    <span>1,158</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Jean-Luc Columbo, Saint Joseph, Rhône </span>
                    <span>1,980</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2010 Paul Jaboulet-Aîné, Hermitage 'La Chapelle', Rhône </span>
                    <span>7,624</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2010 Paul Jaboulet-Aîné, Châteauneuf-du-Pape 'Les Cedres', Rhône </span>
                    <span>2,825</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Pierre y Remy Gauthier, Les Heritiers Cotes du Rhône, Rhône </span>
                    <span>693</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Francois Labet, Pinot Noir 'Ile de Beaute', Corsica </span>
                    <span>814</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 La Cour de Dames , Pinot Noir, Pays d'Oc </span>
                    <span>858</span>
                </div>-->

               <!-- <h3>WHITE WINE</h3>
                <h4>AUSTRALIA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Elderton, Marsanne-Roussanne, Eden Valley </span>
                    <span>860</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2016 Tyrell's Old Winery, Sauvingon Blanc-Semillon, Limestone Coast</span>
                    <span>880</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Jim Barry, Riesling Lodge Hill , Clare Valley </span>
                    <span>1.050</span>
                </div>


                <h4>CHILE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Concha Y Toro, Chardonnay “Reservado”, Central Valley </span>
                    <span>820</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Miguel Torres, Sauvignon Blanc, Central Valley </span>
                    <span>670</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Santa Digna, Chardonnay, Central Valley</span>
                    <span>870</span>
                </div>

                <h4>NEW ZEALAND</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2016 Craggy Range, Sauvignon Blanc, Martinborough </span>
                    <span>1.120</span>
                </div>

                <div class="group-menu group-menu-onlybottle">
                    <span>2016 Kim Crawford , Sauvignon Blanc, Marlborough </span>
                    <span>820</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Ribbonwood, Sauvignon Blanc, Marlborough</span>
                    <span>2.500</span>
                </div>


                <h4>2.500</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>Albert Bichot, Bourgogne “Vieilles Vignes”, Burgundy </span>
                    <span>1.060</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Albert Bichot, Chablis, Burgundy </span>
                    <span>1.170</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2014 Calvet, Bordeaux</span>
                    <span>570</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2014 Chateau Villa Bel-Air, Graves, Bordeaux</span>
                    <span>1.580</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Domaine Chanson, Chablis “Montmains” 1er Cru, Burgundy </span>
                    <span>2.070</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Domaine Chanson, Pouilly-Fuisse, Burgundy </span>
                    <span>1.840</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Gustave Lorentz, Gewurztraminer, Alsace </span>
                    <span>1.320</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Pascal Jolivet, Sancerre, Loire </span>
                    <span>1.040</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2014 Pierre y Remy Gauthier, Cotes du Rhone “Les Heritiers”</span>
                    <span>695</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Sauvion, Sancerre, Loire </span>
                    <span>1.560</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Trimbach, Riesling, Alsace </span>
                    <span>1.480</span>
                </div>

                <h4>ITALY</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Cantina Castelnouvo del Garda, Soave </span>
                    <span>550</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Villa Antinori, Tuscany </span>
                    <span>700</span>
                </div>-->

               <!-- <h3>RED WINE</h3>
                <h4>ARGENTINA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Catena Zapata, Cabernet Sauvignon, Mendoza </span>
                    <span>1.300</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Finca Flichman, Cabernet Sauvignon “Roble”, Mendoza </span>
                    <span>650</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2016 La Finca , Malbec, Mendoza </span>
                    <span>650</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 La Posta, Malbec-Syrah-Bonarda “Tinto” , Mendoza </span>
                    <span>660</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Luca, Malbec-Cabernet blend “Beso de Dante”, Mendoza </span>
                    <span>2.322</span>
                </div>


                <h4>NEW ZEALAND</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Craggy Range, Bordeaux Blend Gimblett Gravels “Te Kahu”, Hawke's Bay </span>
                    <span>1.360</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2014 Kim Crawford , Pinot Noir, Marlborough</span>
                    <span>1.300</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Ribbonwood, Pinot Noir, Marlborough </span>
                    <span>1.200</span>
                </div>


                <h4>AUSTRALIA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Elderton, Cabernet Sauvignon “High Altitude”, Eden Valley </span>
                    <span>980</span>
                </div>

                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 Torbreck, GSM “Steading”, Barossa Valley</span>
                    <span>3585</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Torbreck, Shiraz-Viognier “Descendant”, Barossa Valley</span>
                    <span>1962</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2014 Tyrrell's, Cabernet Sauvignon “Lost Block”, Limestone Coast</span>
                    <span>1100</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Tyrrell's, Pinot Noir, Limestone Coast </span>
                    <span>1100</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Tyrrell's, Shiraz “Rufus Stone”, Heathcote </span>
                    <span>1600</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Vasse Felix, Cabernet Sauvignon, Margaret River </span>
                    <span>1520</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Yangarra Estate, GSM , McClaren Vale </span>
                    <span>1780</span>
                </div>

                <h4>USA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Freemark Abbey, Merlot, Napa Valley </span>
                    <span>1440</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Kendall-Jackson, Cabernet Sauvignon, Sonoma County</span>
                    <span>1440</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 La Crema, Pinot Noir, Sonoma Coast </span>
                    <span>1440</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 Woodbridge by Robert Mondavi, Merlot, California</span>
                    <span>858</span>
                </div>

                <h4>CHILE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>Castillo de Molina, Shiraz, Maule Valley </span>
                    <span>987</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Chateau los Boldos , “ Grand Reserve, Central Valley </span>
                    <span>794</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Echeverria, Cabernet Sauvignon, Central Valley </span>
                    <span>643</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Lapostolle, Merlot, Colchagua Valley </span>
                    <span>869</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Mancura, Merlot “Etnia”, Central Valley </span>
                    <span>561</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2011 Manso de Velasco, Cabernet Sauvignon, Curico</span>
                    <span>2.444</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Miguel Torres, Cabernet Sauvignon “Hemisferio”, Central Valley </span>
                    <span>669</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Miguel Torres, Cabernet Sauvignon “Las Mulas”, Central Valley </span>
                    <span>841</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Miguel Torres, Cabernet Sauvignon “Tormenta”, Central Valley </span>
                    <span>893</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Vina Bisquertt, Pinot Noir “La Joya”, Leyda Valley </span>
                    <span>880</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Vina Leyda, Pinot Noir “Las Brisas Vineyard”, Leyda Valley </span>
                    <span>1.100</span>
                </div>

                <h4>SPAIN</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>Descendientes de J. Palacios, Bierzo 'Petalos', Castille-Leon </span>
                    <span>2.593</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 La Planta, Ribera del Duero, Castille-Leon</span>
                    <span>696</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Torres, Purgatori', Costers del Segre </span>
                    <span>1.962</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2002 Vega Sicilia, Unico Grand Reserva, Ribera del Duero </span>
                    <span>7.200</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2003 Vega Sicilia, Unico Grand Reserva, Ribera del Duero</span>
                    <span>7.200</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Marques de Caceres, Rioja 'Crianza', Rioja </span>
                    <span>537</span>
                </div>

                <h3>RED WINE</h3>
                <h4>FRANCE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Trimbach, Pinot Noir Classic, Alsace </span>
                    <span>1,680</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Château Gruard-Larose, Saint-Julien 'Second Growth', Bordeaux </span>
                    <span>3,783</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Château Le Crock, Saint-Estèphe, Bordeaux </span>
                    <span>2,668</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2011 Château Villa Bel-Air Graves, Bordeaux</span>
                    <span>1,364</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 J.M. Cazes, Pauillac, Bordeaux </span>
                    <span>770</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2012 La Réserve de Malartic-Lagraviere, Pessac-Leognan, Bordeaux</span>
                    <span>1,800</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Le Médoc de Cos d'Estournel, Saint-Estèphe, Bordeaux </span>
                    <span>2,609</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 S de Siran, Margaux, Bordeaux </span>
                    <span>2,200</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2012 Albert Bichot, Cotes de Nuits-Villages, Burgundy</span>
                    <span>1,422</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Albert Bichot, Bourgogne 'Vieilles Vignes',Burgundy </span>
                    <span>1,059</span>
                </div>

                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Domaine Chanson, Santenay, Burgundy </span>
                    <span>1,962</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Domaine de la Romanée-Conti , Grands Echézeaux Grand Cru,Burgundy </span>
                    <span>38,000</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Guigal, Crozes-Hermitage, Rhône </span>
                    <span>1,906</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Guigal, Cotes du, Rhône </span>
                    <span>1,158</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Jean-Luc Columbo, Saint Joseph, Rhône </span>
                    <span>1,980</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> </span>
                    <span></span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2010 Paul Jaboulet-Aîné, Hermitage 'La Chapelle', Rhône </span>
                    <span>7,624</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2010 Paul Jaboulet-Aîné, Châteauneuf-du-Pape 'Les Cedres', Rhône </span>
                    <span>2,825</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Pierre y Remy Gauthier, Les Heritiers Cotes du Rhône, Rhône </span>
                    <span>693</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 Francois Labet, Pinot Noir 'Ile de Beaute', Corsica</span>
                    <span>814</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 La Cour de Dames , Pinot Noir, Pays d'Oc</span>
                    <span>858</span>
                </div>-->


            </div>
            <div class="gallery_laibunfu wines-content wines-content2">
                <h3>EXOTIC WHITE WINES</h3>

                <h3>WHITE WINE </h3>
                <div class="unit">
                    <div class="unit-col"></div>
                    <div class="unit-col"></div>
                    <div class="unit-col">BOTTLE</div>
                </div>
                <h4>AUSTRALIA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Elderton, Marsanne-Roussanne, Eden Valley  </span>
                    <span>860</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2016 Tyrell's Old Winery, Sauvingon Blanc-Semillon, Limestone Coast</span>
                    <span>880</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Jim Barry, Riesling Lodge Hill , Clare Valley </span>
                    <span>1.050</span>
                </div>

                <h4>CHILE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Concha Y Toro, Chardonnay “Reservado”, Central Valley  </span>
                    <span>820</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2014 Miguel Torres, Sauvignon Blanc, Central Valley </span>
                    <span>670</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Santa Digna, Chardonnay, Central Valley </span>
                    <span>870</span>
                </div>

                <h4>NEW ZEALAND</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2016 Craggy Range, Sauvignon Blanc, Martinborough  </span>
                    <span>1.120</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2016 Kim Crawford , Sauvignon Blanc, Marlborough  </span>
                    <span>820</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 Ribbonwood, Sauvignon Blanc, Marlborough </span>
                    <span>2.500</span>
                </div>

                <h4>FRANCE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>Albert Bichot, Bourgogne “Vieilles Vignes”, Burgundy  </span>
                    <span>1.060</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Albert Bichot, Chablis, Burgundy  </span>
                    <span>1.170</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Calvet, Bordeaux  </span>
                    <span>570</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Chateau Villa Bel-Air, Graves, Bordeaux  </span>
                    <span>1.580</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> Domaine Chanson, Chablis “Montmains” 1er Cru, Burgundy </span>
                    <span>2.070</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Domaine Chanson, Pouilly-Fuisse, Burgundy </span>
                    <span>1.840</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Gustave Lorentz, Gewurztraminer, Alsace  </span>
                    <span>1.320</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>Pascal Jolivet, Sancerre, Loire </span>
                    <span>1.040</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Pierre y Remy Gauthier, Cotes du Rhone “Les Heritiers”  </span>
                    <span>695</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Sauvion, Sancerre, Loire  </span>
                    <span>1.560</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Trimbach, Riesling, Alsace  </span>
                    <span>1.480</span>
                </div>
                <h4>ITALY</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Cantina Castelnouvo del Garda, Soave  </span>
                    <span>550</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Villa Antinori, Tuscany  </span>
                    <span>700</span>
                </div>


            </div>
            <div class="gallery_laibunfu wines-content wines-content3">
                <h3>  BOLD RED WINES </h3>
                <h3>RED WINE </h3>
                <div class="unit">
                    <div class="unit-col"></div>
                    <div class="unit-col"></div>
                    <div class="unit-col">BOTTLE</div>
                </div>
                <h4>ARGENTINA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2011 Catena Zapata, Cabernet Sauvignon, Mendoza </span>
                    <span>1.300</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> Finca Flichman, Cabernet Sauvignon “Roble”, Mendoza </span>
                    <span>650</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2016 La Finca , Malbec, Mendoza  </span>
                    <span>650</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 La Posta, Malbec-Syrah-Bonarda “Tinto” , Mendoza </span>
                    <span>660</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> Luca, Malbec-Cabernet blend “Beso de Dante”, Mendoza </span>
                    <span>2.322</span>
                </div>

                <h4>NEW ZEALAND</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Craggy Range, Bordeaux Blend Gimblett Gravels “Te Kahu”, Hawke's Bay  </span>
                    <span>1.360</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2014 Kim Crawford , Pinot Noir, Marlborough </span>
                    <span>1.300</span>
                </div>

                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Ribbonwood, Pinot Noir, Marlborough  </span>
                    <span>1.200</span>
                </div>
                <h4>AUSTRALIA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Elderton, Cabernet Sauvignon “High Altitude”, Eden Valley </span>
                    <span>980</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Torbreck, GSM “Steading”, Barossa Valley  </span>
                    <span>3585</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Torbreck, Shiraz-Viognier “Descendant”, Barossa Valley  </span>
                    <span>1962</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Tyrrell's, Cabernet Sauvignon “Lost Block”, Limestone Coast  </span>
                    <span>1100</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> Tyrrell's, Pinot Noir, Limestone Coast </span>
                    <span>1100</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Tyrrell's, Shiraz “Rufus Stone”, Heathcote  </span>
                    <span>1600</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Vasse Felix, Cabernet Sauvignon, Margaret River  </span>
                    <span>1520</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Yangarra Estate, GSM , McClaren Vale  </span>
                    <span>1780</span>
                </div>


                <h4>USA</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Freemark Abbey, Merlot, Napa Valley </span>
                    <span>1440</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Kendall-Jackson, Cabernet Sauvignon, Sonoma County </span>
                    <span>1440</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>  </span>
                    <span>1440</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 La Crema, Pinot Noir, Sonoma Coast  </span>
                    <span></span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Woodbridge by Robert Mondavi, Merlot, California  </span>
                    <span>858</span>
                </div>

                <h4>CHILE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>Castillo de Molina, Shiraz, Maule Valley </span>
                    <span>987</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 Chateau los Boldos , “ Grand Reserve, Central Valley </span>
                    <span>794</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Echeverria, Cabernet Sauvignon, Central Valley  </span>
                    <span>643</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Lapostolle, Merlot, Colchagua Valley  </span>
                    <span>869</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 Mancura, Merlot “Etnia”, Central Valley  </span>
                    <span>561</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2011 Manso de Velasco, Cabernet Sauvignon, Curico  </span>
                    <span>2.444</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 Miguel Torres, Cabernet Sauvignon “Hemisferio”, Central Valley </span>
                    <span>669</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 Miguel Torres, Cabernet Sauvignon “Las Mulas”, Central Valley </span>
                    <span>841</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 Miguel Torres, Cabernet Sauvignon “Tormenta”, Central Valley </span>
                    <span>893</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Vina Bisquertt, Pinot Noir “La Joya”, Leyda Valley </span>
                    <span>880</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Vina Leyda, Pinot Noir “Las Brisas Vineyard”, Leyda Valley </span>
                    <span>1.100</span>
                </div>

                <h4>SPAIN</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> Descendientes de J. Palacios, Bierzo 'Petalos', Castille-Leon </span>
                    <span>2.593</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 La Planta, Ribera del Duero, Castille-Leon  </span>
                    <span>696</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Torres, Purgatori', Costers del Segre  </span>
                    <span>1.962</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2002 Vega Sicilia, Unico Grand Reserva, Ribera del Duero  </span>
                    <span>7.200</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2003 Vega Sicilia, Unico Grand Reserva, Ribera del Duero  </span>
                    <span>7.200</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2013 Marques de Caceres, Rioja 'Crianza', Rioja  </span>
                    <span>537</span>
                </div>

                <h3>RED WINE</h3>
                <h4>FRANCE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Trimbach, Pinot Noir Classic, Alsace </span>
                    <span>1,680</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Château Gruard-Larose, Saint-Julien 'Second Growth', Bordeaux  </span>
                    <span>3,783</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2012 Château Le Crock, Saint-Estèphe, Bordeaux </span>
                    <span>2,668</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2011 Château Villa Bel-Air Graves, Bordeaux </span>
                    <span>1,364</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2011 J.M. Cazes, Pauillac, Bordeaux </span>
                    <span>770</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2012 La Réserve de Malartic-Lagraviere, Pessac-Leognan, Bordeaux </span>
                    <span>1,800</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2011 Le Médoc de Cos d'Estournel, Saint-Estèphe, Bordeaux </span>
                    <span>2,609</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2012 S de Siran, Margaux, Bordeaux </span>
                    <span>2,200</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2012 Albert Bichot, Cotes de Nuits-Villages, Burgundy </span>
                    <span>1,422</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 Albert Bichot, Bourgogne 'Vieilles Vignes',Burgundy </span>
                    <span>1,059</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2012 Domaine Chanson, Santenay, Burgundy </span>
                    <span>1,962</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> Domaine de la Romanée-Conti , Grands Echézeaux Grand Cru,Burgundy </span>
                    <span>38,000</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2013 Guigal, Crozes-Hermitage, Rhône </span>
                    <span>1,906</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2012 Guigal, Cotes du, Rhône </span>
                    <span>1,158</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2014 Jean-Luc Columbo, Saint Joseph, Rhône  </span>
                    <span>1,980</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2010 Paul Jaboulet-Aîné, Hermitage 'La Chapelle', Rhône  </span>
                    <span>7,624</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2010 Paul Jaboulet-Aîné, Châteauneuf-du-Pape 'Les Cedres', Rhône  </span>
                    <span>2,825</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2014 Pierre y Remy Gauthier, Les Heritiers Cotes du Rhône, Rhône </span>
                    <span>693</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2015 Francois Labet, Pinot Noir 'Ile de Beaute', Corsica </span>
                    <span>814</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>2015 La Cour de Dames , Pinot Noir, Pays d'Oc  </span>
                    <span>858</span>
                </div>

            </div>
            <div class="gallery_laibunfu wines-content wines-content4">
                <h3>SWEET WINES </h3>
                <h4>ALSACE</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1993 Zind Humbrecht, Pinot Gris 'Clos Windsbuhl' </span>
                    <span>6,500</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1994 Zind Humbrecht, Pinot Gris 'Clos St Urbain' V.T  </span>
                    <span>6,200</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>1996 Zind Humbrecht, Pinot Gris 'Heimbourg'  </span>
                    <span>6,800</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1997 Zind Humbrecht, Pinot Gris 'Clos St Urbain' </span>
                    <span>8,500</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1998 Zind Humbrecht, Pinot Gris 'Clos St Urbain' S.G.N.  </span>
                    <span>8,900 </span>
                </div>

                <h4>SAUTERNES</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1975 Château Rieussec, Premier Cru Classe   </span>
                    <span>8,500 </span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>  1975 Château Suduiraut, Premier Cru Classe  </span>
                    <span>8,500 </span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1982 Château Guiraud, Premier Cru Classe  </span>
                    <span>7,200 </span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span>  1986 Château d'Yquem, Premier Cru Superieur Classe  </span>
                    <span>10,500 </span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1987 Château d'Yquem, Premier Cru Superieur Classe   </span>
                    <span>9,500 </span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1988 Château d'Yquem, Premier Cru Superieur Classe   </span>
                    <span>9,800 </span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1989 Château Rieussec, Premier Cru Classe  </span>
                    <span>6,200  </span>
                </div>
                <h4>HUNGARY</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> 2000 Borkulonlegesseg, Tokaji Aszú '6 Puttonyos',Tokaj 37.5cl  </span>
                    <span> 1,400</span>
                </div>

                <h4>SPAIN</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span>1983 Toro Albala, Don Pedro Ximenez Grand Reserva   </span>
                    <span>3,800 </span>
                </div>
                <h4>PORTUGAL</h4>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1977 Dow’s Vintage Oporto  </span>
                    <span>6,800</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1985 Graham’s Vintage Oporto  </span>
                    <span>5,500</span>
                </div>
                <div class="group-menu group-menu-onlybottle">
                    <span> 1994 Graham’s Vintage Oporto  </span>
                    <span>5,200</span>
                </div>
            </div>
        </div>
    </div>

    <?php
    return ob_get_clean();
}

add_shortcode( 'wines-content', 'wines_content_func' );