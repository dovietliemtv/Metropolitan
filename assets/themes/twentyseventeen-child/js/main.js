
(function($){
    $(document).ready(function(){
        $(".menu-link a").click(function(e){
            e.preventDefault();
            var index = $(".menu-link a").index(this) + 1;
            console.log(index);
            $(".menu-content").removeClass("active");
            $(".menu-content"+index).addClass("active");
        });

        $(".wines-link a.tab").click(function(e){
            e.preventDefault();
            var index = $(".wines-link a").index(this) + 1;
            $(".wines-content").removeClass("active");
            $(".wines-content"+index).addClass("active");
        })
        $(".membership-link a.tab").click(function(e){
            e.preventDefault();
            console.log('test');
            var index = $(".membership-link a").index(this) + 1;
            $(".membership-content").removeClass("active");
            $(".membership-content"+index).addClass("active");
        })
    }); //end document ready

})(jQuery);
