<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>-child/images/logo.png"/>
<link rel="profile" href="http://gmpg.org/xfn/11">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri()?>-child/fancybox/jquery-1.4.3.min.js"></script>
    <script src="<?php echo get_template_directory_uri()?>-child/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

	<script src="<?php echo get_template_directory_uri()?>-child/js/main.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>-child/fancybox/jquery.fancybox-1.3.4.css">

    <script>
        jQuery(document).ready(function($){

            $("a[rel=example_group]").fancybox({
                'transitionIn'		: 'none',
                'transitionOut'		: 'none',
                'titlePosition' 	: 'over'
            });

        });

    </script>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

	<div class="menu_laibunfu">
<div class="logo">
	<a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>-child/images/logo.png" Alt="Logo Laibunfu"></a>
</div>
<nav class="dt-main-menu_laibunfu">

	<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</nav><!-- .dt-main-menu -->
<div class="icon-social">
	<ul>
		<li><a href="https://www.facebook.com/met.grill.7?hc_ref=ARRxTxfSOLaBaAgbpGEFK3bbus7hbiJ3OBl258G5WXRXo3Y2yjDglCky0OPmMi68-o8&pnref=story"><img src="<?php echo get_site_url(); ?>/assets/uploads/2017/06/facebook.png"></a></li>
		<!--<li><a href="#"><img src="<?php echo get_site_url(); ?>/assets/uploads/2017/06/twitter.png"></a></li>
		<li><a href="#"><img src="<?php echo get_site_url(); ?>/assets/uploads/2017/06/pinterest.png"></a></li>
		<li><a href="#"><img src="<?php echo get_site_url(); ?>/assets/uploads/2017/06/flickr.png"></a></li>-->
	</ul>
</div>
</div>
<div class="header_mobile">
	<div class="logo_mobile"><a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>-child/images/logo.png" Alt="Logo Laibunfu"></a></div>
	<div class="button_menu">
		<div class="popupmenu_thanh popup_menu_1"></div>
    	<div class="popupmenu_thanh popup_menu_2"></div>
    	<div class="popupmenu_thanh popup_menu_3"></div>
	</div>
	<div class="menu_mobile">
		<nav class="dt-main-menu_laibunfu_mobile">

	<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
		</nav><!-- .dt-main-menu -->
	</div>
</div>

	<div class="site-content-contain">
		<div id="content" class="site-content">
