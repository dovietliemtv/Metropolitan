<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package PhotoBook
 */

?>
	<footer class="dt-footer <?php if ( ! is_front_page() ) { echo 'dt-footer-sep'; } ?>">

		<?php if( is_active_sidebar( 'dt-footer-social' ) ) : ?>

		<div class="container">
			<div class="row">
				<div class="dt-footer-cont">
					<div class="col-lg-12">
						<div class="dt-footer-social">

							<?php dynamic_sidebar( 'dt-footer-social' ); ?>

						</div><!-- .dt-footer-social -->
					</div><!-- .col-lg-3 -->
				</div><!-- .dt-footer-cont -->
			</div><!-- .row -->
		</div><!-- .container -->

		<?php endif; ?>

		<div class="dt-footer-bar">
		</div><!-- .dt-footer-bar -->
	</footer><!-- .dt-footer -->

	<a id="back-to-top" class="transition35"><i class="fa fa-angle-up"></i></a><!-- #back-to-top -->
<div class="footer_1">
	<div class="icon-social">
	<ul>
		<li><a href="https://www.facebook.com/met.grill.7?hc_ref=ARRxTxfSOLaBaAgbpGEFK3bbus7hbiJ3OBl258G5WXRXo3Y2yjDglCky0OPmMi68-o8&pnref=story"><img src="http://laibunfu.alipo.vn/assets/uploads/2017/06/facebook.png"></a></li>
		<!--<li><a href="#"><img src="http://laibunfu.alipo.vn/assets/uploads/2017/06/twitter.png"></a></li>
		<li><a href="#"><img src="http://laibunfu.alipo.vn/assets/uploads/2017/06/pinterest.png"></a></li>
		<li><a href="#"><img src="http://laibunfu.alipo.vn/assets/uploads/2017/06/flickr.png"></a></li>-->
	</ul>
	</div>
	<div class="copyright">© Metropolitan Concept Limited. 2018 All rights reserved.<br><span class="design_by"><a href="http://alipo.vn" target="_blank">Web Design</a> by Fee Creative</span> </div>
	<div>
</div>
<?php wp_footer(); ?>
<script type="text/javascript">
	jQuery(document).ready(function($){
		
		$('.button_menu').click(function(){
			$('.menu_mobile').slideToggle(300);
		});

    });

</script>
</script>
</body>
</html>
