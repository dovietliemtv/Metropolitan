<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       dovietliem.tv
 * @since      1.0.0
 *
 * @package    Booking_Custom
 * @subpackage Booking_Custom/public/partials
 */
?>

    <div class="booking-wrapper">
        <div class="loading">
            <img src="<?php echo plugins_url('booking-custom/includes/images/loading.gif')?>" alt="">
        </div>
        <div class="step_1">
            <div class="booking_option">
                <div class="form_field form_field_col">
                    <label for="adults_num">Adults</label>
                    <select id="adults_num">
                        <option value="1">1</option>
                        <option value="2" selected>2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
                <div class="form_field form_field_col form_field_col2">
                    <label for="child_num">Children</label>
                    <select id="child_num">
                        <option value="0" selected>0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                    </select>
                </div>
            </div>
            <div class="calendar-wrapper">
                <button id="btnPrev" type="button"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                <button id="btnNext" type="button"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <div id="divCal"></div>
                <div id="today-click">
                    <span>Today</span>
                </div>
            </div>
            <div class="booking_option">
                <div class="form_field form_field_col">
                    <div id="date_choose">
                        June 19 2017
                    </div>
                </div>
                <div class="form_field form_field_col form_field_col2">
                    <label for="time_booking">Time</label>
                    <select id="time_booking">
                        <option value="11:00 am">11:00 am</option>
                        <option value="11:15 am">11:15 am</option>
                        <option value="11:30 am">11:30 am</option>
                        <option value="11:45 am">11:45 am</option>
                        <option value="12:00 pm">12:00 pm</option>
                        <option value="12:15 pm">12:15 pm</option>
                        <option value="12:30 pm">12:30 pm</option>
                        <option value="12:45 pm">12:45 pm</option>
                        <option value="1:00 pm">1:00 pm</option>
                        <option value="1:15 pm">1:15 pm</option>
                        <option value="1:30 pm">1:30 pm</option>
                        <option value="1:45 pm">1:45 pm</option>
                        <option value="2:00 pm">2:00 pm</option>

                        <option value="4:30 pm">4:30 pm</option>
                        <option value="5:00 pm">5:00 pm</option>
                        <option value="5:30 pm">5:30 pm</option>
                        <option value="6:00 pm">6:00 pm</option>
                        <option value="6:15 pm">6:15 pm</option>
                        <option value="6:30 pm">6:30 pm</option>
                        <option value="6:45 pm">6:45 pm</option>
                        <option value="7:00 pm">7:00 pm</option>
                        <option value="7:30 pm">7:30 pm</option>
                        <option value="8:00 pm">8:00 pm</option>
                        <option value="8:30 pm">8:30 pm</option>
                        <option value="9:00 pm">9:00 pm</option>
                        <option value="9:30 pm">9:30 pm</option>
                        <option value="10:00 pm">10:00 pm</option>
                    </select>
                </div>
            </div>
            <div class="pre-next-btn">
                <div class="pre-next-btn-col pre-btn">
                    <button class="disable_btn">Previous</button>
                </div>
                <div class="pre-next-btn-col next-btn">
                    <button id="next-btn">Next</button>
                </div>
            </div>
        </div>
        <div class="step_2">
            <div class="info-detail">
                <p>We have a table for you at </p>
                <p><span>Metropolitan</span> for <span><span id="people_num">2</span> people</span></p>
                <p id="time_date_booking">at <span id="time_booking_show">6:30 pm</span> on <span id="date_booking">19 june 2017</span></p>
            </div>
            <div class="name_field">
                <div class="name_field_col name_field_col1">
                    <select id="d-title">
                        <option value="mr." selected>Mr.</option>
                        <option value="ms.">Ms.</option>
                        <option value="mrs.">Mrs.</option>
                        <option value="Mdm.">Mdm.</option>
                    </select>
                </div>
                <div class="name_field_col name_field_col2">
                    <input type="text" name="first-name" id="first-name" placeholder="First Name">
                </div>
                <div class="name_field_col name_field_col3">
                    <input type="text" name="surname" id="surname" placeholder="Surname">
                </div>
            </div>
            <div class="line-form-field">
                <label for="">Email Address*</label>
                <div class="input_col2">
                    <input type="text" name="email" id="email" placeholder="Enter Email Address">

                </div>
            </div>
            <div class="line-form-field">
                <label for="">Mobile Phone*</label>
                <div class="input_col2">
                    <input type="text" class="head-phone-number" name="head-phone-number" id="head-phone-number" value="+84">
                    <input type="text" class="phone-number" name="phone-number" id="phone-number" placeholder="Phone Number">
                </div>
            </div>
            <div class="line-form-field">
                <label for="">Special Requests</label>
                <div class="input_col2">
                    <textarea id="notes" class="form-control" name="notes" placeholder="Message (Maximum 85 characters.)" maxlength="85"></textarea>
                    <p>Special requests are not guaranteed and are subject to availability and restaurant discretion.</p>
                </div>
            </div>

            <div class="conditions">
                <label class="control control--checkbox">I agree to book this reservation in good will. If there are any changes or cancellations, I will do my best to inform the restaurant
                    <input type="checkbox" checked="checked" id="condition"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <div class="pre-next-btn">
                <div class="pre-next-btn-col pre-btn">
                    <button id="previous-btn">Previous</button>
                </div>
                <div class="pre-next-btn-col next-btn">
                    <button id="confirm_booking">Confirm</button>
                </div>
            </div>

            <p id="remind">Please enter all infomation</p>

        </div>
    </div>


<input type="hidden" id="ajaxUrl" value="<?php echo admin_url( 'admin-ajax.php' )?>">




<script>
	(function( $ ) {
		'use strict';
        $(document).ready(function(){

            var dayChoose = '';
            var monthYear = '';

            $('#divCal').on('click', 'table tr td.available-day', function() {
                $('table tr td.available-day').removeClass('available-day-active');
                $(this).addClass('available-day-active');

                dayChoose = $('.available-day-active').text();
                dayChoose = dayChoose.substring(0,2);
                var monthYear = $('#monthChoose').text();


                $('#date_choose').text(dayChoose +' '+ monthYear);

            });

            $('#today-click').on('click',function(){
                location.reload();
            });

            $('#previous-btn').on('click',function(){
                $('.step_2').hide();
                $('.step_1').show();
                $('#remind').fadeOut();

            });
            $('#next-btn').on('click',function(){
                dayChoose = $('.available-day-active').text();
                dayChoose = dayChoose.substring(0,2);


                $('.step_1').hide();
                $('.step_2').show();

                var monthYear = $('#monthChoose').text();
                $('#date_booking').html(dayChoose + ' '+ monthYear);
                $('#time_booking_show').html($('#time_booking').val());

                var peoNum = parseInt($('#adults_num').val()) + parseInt($('#child_num').val());
                $('#people_num').html(peoNum);
            });


//confirm form
            jQuery('#confirm_booking').on('click',function () {

                var adultsNum = parseInt($('#adults_num').val());
                var childNum = parseInt($('#child_num').val());
                var time = $('#time_date_booking').text();
                var CustomerName = $('#d-title').val() + ' '+ $('#first-name').val() + ' ' + $('#surname').val();
                var email = $('#email').val();
                var phone = $('#head-phone-number').val() + ' '+ $('#phone-number').val();
                var notes = $('#notes').val();


                if( $('#d-title').val() != '' && $('#first-name').val() != '' && $('#surname').val() != '' && email != '' &&  $('#phone-number').val()!= ''){
                    if(!validateEmail(email)){
                        $('#remind').html('Email address is not valid');
                        $('#remind').fadeIn();
                    }else {
                        if(!$('#condition').is(':checked')){
                            $('#remind').html('You have to agree to the Terms and Conditions');
                            $('#remind').fadeIn();
                        }else {
                            if(!validatePhone($('#phone-number').val())){
                                $('#remind').html('Phone number is not valid');
                                $('#remind').fadeIn();
                            }else {
                                $('#remind').fadeOut();
                                $('.loading').css('display','flex');
                                jQuery.post(
                                    $('#ajaxUrl').val(),
                                    {
                                        'action': 'email_booking',
                                        'data':   'foobarid',
                                        'adultsNum': adultsNum,
                                        'childNum': childNum,
                                        'time': time,
                                        'customerName': CustomerName,
                                        'email': email,
                                        'phone': phone,
                                        'notes': notes
                                    },
                                    function(response){
                                        $('.step_2').html('<h2 class="response">'+ response + '</h2>')
                                        console.log(response);
                                        $('.loading').hide();

                                    }
                                );
                            }
                        }

                    }
                }else{
                    $('#remind').fadeIn();
                }



            })

// end confirm form
		});//end document ready

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        function validatePhone(number) {
            var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
            return re.test(number);
        }

	})( jQuery );





</script>
<?php

?>
