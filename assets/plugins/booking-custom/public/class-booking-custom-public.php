<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       dovietliem.tv
 * @since      1.0.0
 *
 * @package    Booking_Custom
 * @subpackage Booking_Custom/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Booking_Custom
 * @subpackage Booking_Custom/public
 * @author     liemdotv <dovietliemtv@gmail.com>
 */
class Booking_Custom_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->addShortcode();
		$this->ajaxFunctionPublic();
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Booking_Custom_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Booking_Custom_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

 		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/booking-custom-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Booking_Custom_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Booking_Custom_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/booking-custom-public.js', array( 'jquery' ), $this->version, false );



	}
	public function event_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/booking-main-event-js.js', array( 'jquery' ), $this->version, false );
	}
	public function addShortcode(){

		add_shortcode( 'booking_custom', 'bookingCustom' );
		 function bookingCustom(){
	    	include(plugin_dir_path( __FILE__ ). 'partials/booking-custom-public-display.php'); 
			}
	}

	public function ajaxFunctionPublic(){

        add_action( 'wp_ajax_nopriv_email_booking', 'sent_email_booking' );
        add_action( 'wp_ajax_email_booking', 'sent_email_booking' );

        function sent_email_booking() {

            $adultsNum = $_POST['adultsNum'];
            $childNum = $_POST['childNum'];
            $time = $_POST['time'];
            $customerName = $_POST['customerName'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $notes = $_POST['notes'];

            $content = '
            Adults: '.$adultsNum. '
            Children: '.$childNum.'
            Time: '.$time.'
            Customer Name: '.$customerName.'
            Email address: '.$email.'
            Phone number: '.$phone.'
            Notes: '.$notes;

            $replyContent = 'Thank you for choosing the Metropolitan Grill in Saigon.  We will contact you within 24 hours to confirm your reservations.  
            If you have any further needs please don\'t hesitate to call us directly at 0283.821.2757.  
            Have a great day and enjoy your time at the Met.  
            We look forward to serving you.';

            wp_mail( 'reservation@metropolitan.vn','Booking', $content, $headers = 'Metropolitan' );
            wp_mail( $email,'Thank you for choosing the Metropolitan Grill in Saigon.', $replyContent, $headers = 'Metropolitan' );

            echo 'Thank you for booking.<br> We will contact to you soon!';
            // Don't forget to stop execution afterward.
            wp_die();
        }

    }
	

	

}
