(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	 var Cal = function(divId) {

  //Store div id
  this.divId = divId;

  // Days of week, starting on Sunday
  this.DaysOfWeek = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat'
  ];
var today = 0;
var currentMonth = '';
var currentYear = '';
  // Months, stating on January
  this.Months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];

  // Set the current month, year
  var d = new Date();

  this.currMonth = d.getMonth();
  this.currYear = d.getFullYear();
  this.currDay = d.getDate();

};

// Goes to next month
Cal.prototype.nextMonth = function() {
  if ( this.currMonth == 11 ) {
    this.currMonth = 0;
    this.currYear = this.currYear + 1;
  }
  else {
    this.currMonth = this.currMonth + 1;
  }
  this.showcurr();
};

// Goes to previous month
Cal.prototype.previousMonth = function() {

    if (this.currMonth == 0) {
      this.currMonth = 11;
      this.currYear = this.currYear - 1;
    }
    else {
      this.currMonth = this.currMonth - 1;
    }
    this.showcurr();


};


// Show current month
Cal.prototype.showcurr = function() {
  this.showMonth(this.currYear, this.currMonth);
};

// Show month (year, month)
Cal.prototype.showMonth = function(y, m) {

  var d = new Date()
  // First day of the week in the selected month
  , firstDayOfMonth = new Date(y, m, 1).getDay()
  // Last day of the selected month
  , lastDateOfMonth =  new Date(y, m+1, 0).getDate()
  // Last day of the previous month
  , lastDayOfLastMonth = m == 0 ? new Date(y-1, 11, 0).getDate() : new Date(y, m, 0).getDate();

	var popupNomal = '<div class="icon-down-wraper">'+
												'<div class="icon-down-popup"></div>'+
										'</div>' +
										'<div class="popup-wrapper">' +
												'<ul>' +
														'<li>Reservation Hours</li>' +
														'<li>11:00 am - 2:00 pm</li>' +
														'<li>4:30 pm - 10:00 pm</li>' +
												'</ul>'+
										'</div>';

    var popupSunday = '<div class="icon-down-wraper">'+
        '<div class="icon-down-popup popupsunday"></div>'+
        '</div>' +
        '<div class="popup-wrapper popupsunday">' +
        '<ul>' +
        '<li>Please contact the </li>' +
        '<li>Restaurant directly</li>' +
        '</ul>'+
        '</div>';
  var html = '<table>';

  // Write selected month and year
  html += '<thead><tr>';
  html += '<td colspan="7" id="monthChoose">' + this.Months[m] + ' ' + y + '</td>';
  html += '</tr></thead>';



  // Write the header of the days of the week
  html += '<tr class="days">';
  for(var i=0; i < this.DaysOfWeek.length;i++) {

    html += '<td>' + this.DaysOfWeek[i] + '</td>';
  }
  html += '</tr>';

  // Write the days
  var i=1;
  do {

      var iString = i.toString();
      if(iString.length<2){
          iString = '0'+iString;
      }
    var dow = new Date(y, m, i).getDay();

    // If Sunday, start new row
    if ( dow == 0 ) {
      html += '<tr>';
    }
    // If not Sunday but first day of the month
    // it will write the last days from the previous month
    else if ( i == 1 ) {
      html += '<tr>';
      var k = lastDayOfLastMonth - firstDayOfMonth+1;

      for(var j=0; j < firstDayOfMonth; j++) {
          var kString = k.toString();
          if(kString.length<2){
              kString = '0'+ kString;
          }else {
              kString = k.toString();
          }
        html += '<td class="not-current">' + kString + '</td>';
        k++;
      }
    }


    // Write the current day in the loop
    var chk = new Date();
    var chkY = chk.getFullYear();
    var chkM = chk.getMonth();

    if(dow == 0 && chkY == this.currYear && chkM == this.currMonth && i == this.currDay){
        html += '<td class="today sunday" >' + iString + popupSunday +'</td>';
    }else  if(chkY == this.currYear && dow == 0){
        if(chkM > this.currMonth){
            html += '<td class="normal">' + iString + popupSunday+ '</td>';
        }
        if(chkM < this.currMonth && dow ==0){
            html += '<td class="normal sunday">' + iString + popupSunday+ '</td>';
        }
        if(chkM == this.currMonth && dow==0){
            if(i >= this.currDay && dow == 0){
                html += '<td class="normal sunday">' + iString + popupSunday+ '</td>';
            }
            if(i < this.currDay && dow == 0){
                html += '<td class="normal">' + iString + popupSunday+ '</td>';
            }
        }
    }
      if(chkY > this.currYear && dow == 0){
          html += '<td class="normal">' + iString + popupSunday+ '</td>';
      }
      if(chkY < this.currYear && dow == 0){
          html += '<td class="normal sunday">' + iString + popupSunday+ '</td>';
      }else if(dow != 0){
// start not sunday
          if (chkY == this.currYear && chkM == this.currMonth && i == this.currDay) {
              html += '<td class="today available-day available-day-active" >' + iString + popupNomal +'</td>';
          }else  if(chkY == this.currYear){
              if(chkM > this.currMonth){
                  html += '<td class="normal">' + iString + popupNomal+ '</td>';
              }
              if(chkM < this.currMonth){
                  html += '<td class="normal available-day">' + iString + popupNomal+ '</td>';
              }
              if(chkM == this.currMonth){
                  if(i >= this.currDay){
                      html += '<td class="normal available-day">' + iString + popupNomal+ '</td>';
                  }
                  if(i < this.currDay){
                      html += '<td class="normal">' + iString + popupNomal+ '</td>';
                  }
              }
          }
          if(chkY > this.currYear){
              html += '<td class="normal">' + iString + popupNomal+ '</td>';
          }
          if(chkY < this.currYear){
              html += '<td class="normal available-day">' + iString + popupNomal+ '</td>';
          }


      }








    // If Saturday, closes the row
    if ( dow == 6 ) {
      html += '</tr>';
    }
    // If not Saturday, but last day of the selected month
    // it will write the next few days from the next month
    else if ( i == lastDateOfMonth ) {
      var k=1;

      for(dow; dow < 6; dow++) {
          var kString = k.toString();
          if(kString.length<2){
              kString = '0'+kString;
          }else {
              kString = k.toString();
          }
        html += '<td class="not-current">' + kString + '</td>';
        k++;
      }
    }

    i++;
  }while(i <= lastDateOfMonth);

  // Closes table
  html += '</table>';

  // Write HTML to the div
    if($("body").hasClass("page-id-88")){
        document.getElementById(this.divId).innerHTML = html;
    }
}


// On Load of the window
window.onload = function() {

  // Start calendar
  var chk = new Date();
  var chkY = chk.getFullYear();
  var c = new Cal("divCal");
  c.showcurr();

  // Bind next and previous button clicks
  getId('btnNext').onclick = function() {
    c.nextMonth();
  };
  getId('btnPrev').onclick = function() {
      c.previousMonth();
  };
};

// Get element by id
function getId(id) {
  return document.getElementById(id);
}

//end calendar


//start custom code
$(document).ready(function(){



    var d = new Date();

    // Months, stating on January
    var Months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];

    var currMonth = d.getMonth();
    var currYear = d.getFullYear();
    var currDay = d.getDate();
    $('#date_choose').text(currDay + ' '+Months[currMonth]+' '+ currYear);

});


})( jQuery );

