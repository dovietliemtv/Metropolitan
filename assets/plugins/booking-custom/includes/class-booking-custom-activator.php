<?php

/**
 * Fired during plugin activation
 *
 * @link       dovietliem.tv
 * @since      1.0.0
 *
 * @package    Booking_Custom
 * @subpackage Booking_Custom/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Booking_Custom
 * @subpackage Booking_Custom/includes
 * @author     liemdotv <dovietliemtv@gmail.com>
 */
class Booking_Custom_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
