<?php

/**
 * Fired during plugin deactivation
 *
 * @link       dovietliem.tv
 * @since      1.0.0
 *
 * @package    Booking_Custom
 * @subpackage Booking_Custom/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Booking_Custom
 * @subpackage Booking_Custom/includes
 * @author     liemdotv <dovietliemtv@gmail.com>
 */
class Booking_Custom_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
